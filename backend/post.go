package backend

import (
	"crypto/rand"
	"errors"
	"fmt"
	"log"
	"math"
	"math/big"
	"os"
	"path"
	"regexp"
	"strings"
	"time"

	"codeberg.org/dralmaember/spdp-server/indexer"
)

var headerRx regexp.Regexp
var groupRx regexp.Regexp

func ExecPost(message []byte, hash string) error {
	msg := string(message)
	separated := strings.SplitN(msg, "\r\n\r\n", 2)
	if len(separated) != 2 {
		return errors.New("Invalid message")
	}
	headersAsStr := separated[0]
	contents := []byte(separated[1])

	headers, err := processHeaders(headersAsStr)
	if err != nil {
		return err
	}

	// find an unused ID
	var id1, id2 uint64
	var idstr string
	group, ok := headers["Group"]
	if !ok || !groupRx.MatchString(group) {
		return errors.New("Invalid group")
	}

	err = os.MkdirAll(group, os.ModePerm|os.ModeDir)
	if err != nil {
		log.Fatalln(err.Error())
	}

	var pth string
	for {
		id1Big, err := rand.Int(rand.Reader, big.NewInt(math.MaxInt64))
		if err != nil {
			log.Fatalln("Getting random number failed:", err.Error())
		}
		id2Big, err := rand.Int(rand.Reader, big.NewInt(math.MaxInt64))
		if err != nil {
			log.Fatalln("Getting random number failed:", err.Error())
		}
		id1, id2 = id1Big.Uint64(), id2Big.Uint64()

		idstr = fmt.Sprintf("%d_%d", id1, id2)

		pth = path.Join(group, idstr)

		if _, err := os.Stat(pth); err != nil {
			break
		}
	}

	if !hasHeader(headers, "Subject") ||
		!hasHeader(headers, "From") {
		return errors.New("Invalid message")
	}

	postedAt := time.Now()
	headers["Identifier"] = idstr
	headers["Posted-At"] = fmt.Sprint(postedAt.Unix())
	headers["Client-Id"] = hash

	err = saveMessage(pth, contents, headers)
	if err != nil {
		return errors.New("Failed to save message")
	}

	if !hasHeader(headers, "In-Reply-To") {
		headers["In-Reply-To"] = ""
	}
	err = indexer.AddToIndex(indexer.IndexEntry{
		Id:          headers["Identifier"],
		Group:       headers["Group"],
		DatePosted:  postedAt,
		IdInReplyTo: headers["In-Reply-To"],
		ClientId:    hash,
		From:        headers["From"],
		Subject:     headers["Subject"],
	})

	if err != nil {
		return errors.New("Failed to save to index")
	} else {
		return nil
	}
}

func saveMessage(pth string, contents []byte, headers map[string]string) error {
	f, err := os.Create(pth)
	defer f.Close()
	if err != nil {
		return err
	}
	for name, val := range headers {
		fmt.Fprintf(f, "%s: %s\r\n", name, val)
	}
	fmt.Fprint(f, "\r\n")
	f.Write(contents)
	return nil
}

func hasHeader(headers map[string]string, header string) bool {
	_, ok := headers[header]
	return ok
}
