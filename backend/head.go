package backend

import (
	"errors"
	"net"
	"os"
	"path"
	"strings"

	"codeberg.org/dralmaember/tldmsg"
)

func ExecHead(conn net.Conn, msg tldmsg.Message) error {
	content := string(msg.Content)
	params := strings.Split(content, " ")
	if len(params) < 2 {
		return errors.New("Invalid request")
	}

	group := params[0]
	id := params[1]

	if !groupRx.MatchString(group) {
		return errors.New("Invalid group name")
	}
	if !idRx.MatchString(id) {
		return errors.New("Invalid ID")
	}

	postBuf, err := os.ReadFile(path.Join(group, id))
	if err != nil {
		return errors.New("Failed to read message. It might not exist.")
	}
	post := string(postBuf)

	splitted := strings.SplitN(post, "\r\n\r\n", 2)
	headers := splitted[0]

	reply := tldmsg.Message{
		MType:   mtypeServerRespSuccess,
		Content: []byte(headers),
	}
	err = reply.WriteToStream(conn)
	if err != nil {
		return errors.New("Failed to reply")
	}

	return nil
}
