package backend

import (
	"errors"
	"regexp"
	"strings"
)

const (
	mtypeServerRespSuccess = 0
	mtypeServerRespFailure = 1
)

var idRx regexp.Regexp

func init() {
	headerRx = *regexp.MustCompile("^[a-zA-Z\\-]+$")
	groupRx = *regexp.MustCompile("^[a-zA-Z0-1\\.]+$")
	idRx = *regexp.MustCompile("^[0-9]+_[0-9]+$")
}

func processHeaders(txt string) (map[string]string, error) {
	out := make(map[string]string)
	splitted := strings.Split(txt, "\r\n")
	for _, header := range splitted {
		sepd := strings.SplitN(header, ":", 2)
		if len(sepd) < 2 || !headerRx.MatchString(sepd[0]) {
			return nil, errors.New("Invalid header")
		}
		out[sepd[0]] = strings.Trim(sepd[1], " \t")
	}

	return out, nil
}
