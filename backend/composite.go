package backend

import (
	"errors"
	"fmt"
	"net"
	"strings"
	"time"

	"codeberg.org/dralmaember/spdp-server/indexer"
	"codeberg.org/dralmaember/tldmsg"
)

func ExecComposite(conn net.Conn, msg tldmsg.Message) error {
	reqStr := string(msg.Content)
	reqSli := strings.Split(reqStr, " ")
	if len(reqSli) != 4 {
		return errors.New("Invalid request")
	}

	group := reqSli[0]
	if !groupRx.MatchString(group) {
		return errors.New("Invalid group")
	}

	var minTime, maxTime uint64
	_, err := fmt.Sscan(reqSli[1], &minTime)
	if err != nil {
		return errors.New("Invalid minimum time")
	}
	_, err = fmt.Sscan(reqSli[2], &maxTime)
	if err != nil {
		return errors.New("Invalid maximum time")
	}

	var limit uint64
	_, err = fmt.Sscan(reqSli[3], &limit)
	if err != nil {
		return errors.New("Invalid limit")
	}

	entries, err := indexer.GetEntriesBetween(
		time.Unix(int64(minTime), 0),
		time.Unix(int64(maxTime), 0),
		limit,
	)
	if err != nil {
		return errors.New("Error while querying the index")
	}

	respBuf := ""

	for _, entry := range entries {
		if entry.IdInReplyTo == "" {
			entry.IdInReplyTo = "-"
		}
		respBuf += entry.Id + "\r\n" +
			fmt.Sprint(entry.DatePosted.Unix()) + "\r\n" +
			entry.IdInReplyTo + "\r\n" +
			entry.ClientId + "\r\n" +
			entry.From + "\r\n" +
			entry.Subject + "\r\n" + "\r\n"
	}

	resp := tldmsg.Message{
		MType:   mtypeServerRespSuccess,
		Content: []byte(respBuf),
	}

	err = resp.WriteToStream(conn)
	if err != nil {
		return errors.New("Failed to reply")
	}

	return nil
}
