package backend

import (
	"errors"
	"net"
	"os"

	"codeberg.org/dralmaember/tldmsg"
)

func ExecRead(conn net.Conn, msg tldmsg.Message) error {
	if !groupRx.Match(msg.Content) {
		return errors.New("Invalid group ID.")
	}

	files, err := os.ReadDir(string(msg.Content))
	if err != nil {
		return errors.New("Couldn't read group. It might not exist.")
	}

	ids := ""
	for _, file := range files {
		ids += file.Name() + "\r\n"
	}
	resp := tldmsg.Message{
		MType:   mtypeServerRespSuccess,
		Content: []byte(ids),
	}
	err = resp.WriteToStream(conn)
	if err != nil {
		return errors.New("Connection issue, failed.")
	}
	return nil
}
