package backend

import (
	"errors"
	"net"
	"os"
	"path"
	"strings"

	"codeberg.org/dralmaember/tldmsg"
)

func ExecGet(conn net.Conn, msg tldmsg.Message) error {
	split := strings.SplitN(string(msg.Content), " ", 2)
	if len(split) != 2 {
		return errors.New("Invalid request")
	}
	if !groupRx.MatchString(split[0]) {
		return errors.New("Invalid group name")
	}
	if !idRx.MatchString(split[1]) {
		return errors.New("Invalid ID")
	}

	pth := path.Join(split...)

	in, err := os.ReadFile(pth)
	if err != nil {
		return errors.New("Failed to read message. It might not exist.")
	}

	resp := tldmsg.Message{
		MType:   mtypeServerRespSuccess,
		Content: in,
	}

	err = resp.WriteToStream(conn)
	if err != nil {
		return errors.New("Failed to reply")
	}

	return nil
}
