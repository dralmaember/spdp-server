module codeberg.org/dralmaember/spdp-server

go 1.17

require github.com/pelletier/go-toml v1.9.4

require codeberg.org/dralmaember/tldmsg v1.0.6

require github.com/mattn/go-sqlite3 v1.14.9
