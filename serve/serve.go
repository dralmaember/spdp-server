package serve

import (
	"crypto/rand"
	"crypto/tls"
	"log"
	"os"

	"codeberg.org/dralmaember/spdp-server/indexer"
)

type ServerConfig struct {
	Address string
	Cert    string
	Key     string
}

type PathConfig struct {
	DataDir string
	IndexDb string
}

type Config struct {
	Server ServerConfig
	Paths  PathConfig
}

func StartServer(cfg *Config) {
	err := indexer.InitIndex(cfg.Paths.IndexDb)
	if err != nil {
		log.Fatalln("Failed to initialise database connection", err.Error())
	}

	cert := loadCert(cfg.Server.Cert, cfg.Server.Key)
	conf := tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientAuth:   tls.RequireAnyClientCert,
	}
	conf.Rand = rand.Reader

	addrStr := cfg.Server.Address
	log.Printf("Starting server at '%s'\n", addrStr)

	ln, err := tls.Listen("tcp", addrStr, &conf)
	if err != nil {
		log.Fatalf("Failed to start socket: %s\n", err.Error())
	}

	os.Chdir(cfg.Paths.DataDir)
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatalln(err.Error())
		}

		tlscon := conn.(*tls.Conn)
		err = tlscon.Handshake()
		if err != nil {
			// Yes, we're just gonna ignore failures
			// It's past 8pm and I'm too tired to care
			// Should work.
			continue
		}

		log.Printf("Incoming connection accepted from %s\n",
			conn.RemoteAddr().String())

		go handleClient(conn, tlscon.ConnectionState().PeerCertificates[0])
	}
}

func loadCert(path, keypath string) tls.Certificate {
	cert, err := tls.LoadX509KeyPair(path, keypath)
	if err != nil {
		log.Fatalln("Error loading certificate", path, "or key file",
			keypath)
	}

	return cert
}
