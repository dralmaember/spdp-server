package serve

import (
	"crypto/sha512"
	"crypto/x509"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"net"

	"codeberg.org/dralmaember/tldmsg"

	"codeberg.org/dralmaember/spdp-server/backend"
)

const (
	mtypePost          = 1
	mtypeRead          = 2
	mtypeHead          = 3
	mtypeGet           = 4
	mtypeCompositeRead = 5
)

func sendSuccessMessage(conn net.Conn) error {
	msg := tldmsg.Message{
		MType:   0,
		Content: []byte("Success!"),
	}
	return msg.WriteToStream(conn)
}

func sendFailureMessage(conn net.Conn, err error) error {
	msg := tldmsg.Message{
		MType:   1,
		Content: []byte(err.Error()),
	}
	return msg.WriteToStream(conn)
}

func handleClient(conn net.Conn, cert *x509.Certificate) {
	defer conn.Close()

	hashBytes := sha512.Sum512(cert.Raw)
	hash := base64.StdEncoding.EncodeToString(hashBytes[:])

	var err error = nil
	for err == nil {
		var msg tldmsg.Message
		msg, err = tldmsg.ReadFromStream(conn)
		if err == nil {
			err, sendReply := interpretMessage(msg, conn, hash)

			if err == nil {
				// Success
				if sendReply {
					err = sendSuccessMessage(conn)
				}
			} else {
				// Failure
				err = sendFailureMessage(conn, err)
			}

			if err != nil {
				break
			}
		} else {
			break
		}
	}
	fmt.Println(err.Error())

	log.Println("Connection terminated,",
		conn.RemoteAddr().String(), "isn't connected anymore")
}

func interpretMessage(msg tldmsg.Message, conn net.Conn, hash string) (error, bool) {
	switch msg.MType {
	case mtypePost:
		return backend.ExecPost(msg.Content, hash), true
	case mtypeRead:
		return backend.ExecRead(conn, msg), false
	case mtypeHead:
		return backend.ExecHead(conn, msg), false
	case mtypeGet:
		return backend.ExecGet(conn, msg), false
	case mtypeCompositeRead:
		return backend.ExecComposite(conn, msg), false
	default:
		return errors.New("No such mtype!"), true
	}
}
