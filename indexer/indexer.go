package indexer

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

var idb *sql.DB

type IndexEntry struct {
	Id          string
	Group       string
	DatePosted  time.Time
	IdInReplyTo string
	ClientId    string
	From        string
	Subject     string
}

func InitIndex(dbpath string) error {
	db, err := sql.Open("sqlite3", dbpath)
	if err != nil {
		return err
	}
	idb = db

	_, err = idb.Exec(`
	CREATE TABLE IF NOT EXISTS "index" (
		"id" TEXT NOT NULL,
		"group" TEXT NOT NULL,
		"date_posted" INTEGER NOT NULL,
		"id_in_reply_to" TEXT NOT NULL,
		"client_id" TEXT NOT NULL,
		"from" TEXT NOT NULL,
		"subject" TEXT NOT NULL
	);
	`)

	if err != nil {
		log.Fatalln("Failed to create table index:", err.Error())
	}

	return nil
}

func CloseIndex() {
	if err := idb.Close(); err != nil {
		log.Fatalln("Failed to close index database:", err.Error())
	}
}

func AddToIndex(entry IndexEntry) error {
	stmt, err := idb.Prepare(`
	INSERT INTO "index"
	("id", "group", "date_posted", "id_in_reply_to", "client_id", "from", "subject") VALUES
	(?,    ?,       ?,             ?,                ?,           ?,      ?)
	`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(
		entry.Id,
		entry.Group,
		entry.DatePosted.Unix(),
		entry.IdInReplyTo,
		entry.ClientId,
		entry.From,
		entry.Subject,
	)
	if err != nil {
		return err
	}

	return nil
}

func GetEntriesBetween(begin, end time.Time, max uint64) ([]IndexEntry, error) {
	var opt string
	if max > 0 {
		opt = fmt.Sprint("LIMIT ", max)
	} else {
		opt = ""
	}
	stmt, err := idb.Prepare(`
	SELECT * FROM "index" WHERE "date_posted" BETWEEN ? AND ? ORDER BY "date_posted" DESC 
	` + opt + ";")
	if err != nil {
		log.Println("Error while querying database:", err.Error())
		return nil, err
	}

	rows, err := stmt.Query(begin.Unix(), end.Unix())
	if err != nil {
		return nil, err
	}

	res := make([]IndexEntry, 0)

	for rows.Next() {
		entry := IndexEntry{}
		var timeAsInt int64
		err := rows.Scan(
			&entry.Id,
			&entry.Group,
			&timeAsInt,
			&entry.IdInReplyTo,
			&entry.ClientId,
			&entry.From,
			&entry.Subject,
		)
		entry.DatePosted = time.Unix(timeAsInt, 0)
		if err != nil {
			log.Fatalln("Failed to read from row.")
		}
		res = append(res, entry)
	}
	return res, nil
}
