package main

import (
	"log"
	"os"

	"github.com/pelletier/go-toml"

	"codeberg.org/dralmaember/spdp-server/serve"
)

func main() {
	confpath := os.Getenv("SPDPD_CFG")
	if confpath == "" {
		confpath = "/etc/spdpd/conf.toml"
	}
	file, err := os.ReadFile(confpath)
	if err != nil {
		log.Fatalf("Configuration file %s not found!\n", confpath)
	}

	var config serve.Config
	err = toml.Unmarshal(file, &config)
	if err != nil {
		log.Fatalln("Failed to parse configuration file")
	}

	serve.StartServer(&config)
}
